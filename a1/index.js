
//1.

let students = ["John", "Joe", "Jane", "Jessie"]

const addToEnd = (arr, element) => {
	if(typeof element !== "string"){
		console.log("Error - can only add strings to the array")
	}else{
		arr.push(element)
		console.log(arr)
	}
}

addToEnd(students, "Joey")
addToEnd(students, 2)


//2.

const addToStart = (arr, element) => {
	if(typeof element !== "string"){
		console.log("Error - can only add strings to the array")
	}else{
		arr.unshift(element)
		console.log(arr)
	}
}

addToEnd(students, "Jimmy")
addToEnd(students, 24)

//3.

const elementChecker = (arr, elementToCheck) => {
	if(arr.length === 0){
		console.log("Error - passed in array is empty")
	}else{
		let result = arr.some(element => 
			element === elementToCheck
		)
		console.log(result)
	}
}

elementChecker(students, "Jane")
elementChecker([], "Jane")

//4.

const checkAllStringsEnding = (arr, char) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else if(char.length > 1){
		console.log("Error - Second argument must be a single character")
	}else{
		let result = arr.every(element => element[element.length-1] === char)
		console.log(result)
	}
}

checkAllStringsEnding(students, "e")
checkAllStringsEnding([], "e")
checkAllStringsEnding(["Jane", 02], "e")



//5.
const stringLengthSorter = (arr) => {
	if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else{
		arr.sort((elementA, elementB) => {
			return elementA.length - elementB.length
		})

		console.log(arr)
	}
}

stringLengthSorter(students)
stringLengthSorter([037, "Ryan"])

//6.

const startsWithCounter = (arr, char) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else if(char.length > 1){
		console.log("Error - Second argument must be a single character")
	}else{
		let result = 0

		arr.forEach(element => {
			if(element[0].toLowerCase() === char.toLowerCase()){
				result++
			}
		})

		console.log(result)
	}
}

startsWithCounter(students, "j")

//.7
const likeFinder = (arr, str) => {
if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else if(arr.some(element => typeof element !== "string")){
		console.log("Error - All array elements must be strings")
	}else if(typeof str !== "string"){
		console.log("Error - Second argument must be of data type string")
	}else{
		let result = []

		arr.forEach(element => {
			if(element.toLowerCase().includes(str.toLowerCase())){
				result.push(element)
			}
		})

		console.log(result)
	}	
}

likeFinder(students, "jo")


//8.
const randomPicker = (arr) => {
	if(arr.length === 0){
		console.log("Error - Passed in array is empty")
	}else{
		console.log(arr[Math.floor(Math.random() * arr.length)])
	}
}

randomPicker(students)